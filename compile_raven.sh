#!/usr/bin/bash

#set -v

# module purge
# module load intel/2023.1.0.x
# module load impi/2021.9
# module load mkl/2023.1
# module load cuda/11.4
# module load nccl/2.17.1

source load_modules.sh

CUDA_LIBS="-L$CUDA_HOME/lib64 -lcublas -lcudart -lrt -Wl,-rpath,$CUDA_HOME/lib64"

echo $NCCL_HOME
NCCL_LIBS="-L$NCCL_HOME/lib -lnccl -Wl,-rpath,$NCCL_HOME/lib"

MKL_LIBS="-L$MKL_HOME/lib/intel64 -lmkl_scalapack_lp64 -lmkl_gf_lp64 -lmkl_sequential -lmkl_core -lmkl_blacs_openmpi_lp64 -lpthread -Wl,-rpath,$MKL_HOME/lib/intel64"

FileName=main
mpicc -O0 -I$CUDA_HOME/include -I$NCCL_HOME/include -o $FileName $FileName.c $MKL_LIBS $CUDA_LIBS $NCCL_LIBS
