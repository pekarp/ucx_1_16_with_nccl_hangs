# ucx_1_16_with_nccl_hangs

This is a self-contained reproducer for the hang-issue with UCX 1.16 + NCCL. It uses MKL ScaLAPACK `pztranc` routine (transpose of distributed complex double precision matrix; similar behavior was observed with the Reference ScaLAPACK) in conjunction with `ncclReduce` or `ncclAllReduce`.

```
git clone https://gitlab.mpcdf.mpg.de/pekarp/ucx_1_16_with_nccl_hangs.git
cd ucx_1_16_with_nccl_hangs
bash compile_raven.sh
sbatch job_raven.sh
```

Observed behavior: the job hangs indefinitely (confirmed timeout after 8 hours).

The hang can be cured in two ways:

1. Add an `MPI_Barrier` after `pztranc`

2. Set an environment variable `export UCX_PROTO_ENABLE=n`