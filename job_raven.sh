#!/bin/sh
##SBATCH -o ./job.out.%j
##SBATCH -e ./job.err.%j
# Initial working directory:
#SBATCH -D ./
# Job name
#SBATCH -J ucx_1_16_with_nccl

#SBATCH --nodes=1
#SBATCH --constraint="gpu"
##SBATCH --partition="gpudev"

#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=18

#SBATCH --gres=gpu:a100:2
#SBATCH --mem=250000

#SBATCH --mail-type=all
#SBATCH --time=00:01:00

localdir=$(dirname $(scontrol show job $SLURM_JOBID | awk -F= '/Command=/{print $2}' | head -n 1))
echo $localdir

source $localdir/load_modules.sh

set -v

export LD_LIBRARY_PATH=$MKL_HOME/lib/intel64:$LD_LIBRARY_PATH

#export UCX_PROTO_ENABLE=n # uncommenting this resolves the hang
#export NCCL_DEBUG=INFO

srun ./main
