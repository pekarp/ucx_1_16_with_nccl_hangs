#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <math.h>
#include <complex.h>
#include <assert.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include "nccl.h"


//__________________________________________________________________________________________________
// blacs functions

void   Cblacs_pinfo( int* mypnum, int* world_size);
void   Cblacs_get( int context, int request, int* value);
int    Cblacs_gridinit( int* context, char * order, int np_row, int np_col);
void   Cblacs_gridinfo( int context, int*  np_row, int* np_col, int*  my_row, int*  my_col);
void   Cblacs_gridexit( int context);
void   Cblacs_exit( int error_code);

// scalapack functions
// find size of the local matrix (NUMber of Rows Or Columns), m_loc or n_loc
int  numroc_( int *N, int *NB, int *iproc, int *isrcproc, int *world_size);

// initialize the matrix descriptor
void descinit_( int *desc, int *M, int *N, int *mb, int *NB, int *irsrc, int *icsrc, int *ictxt, int *lld, int *info);

void pztranc_ (int *m , int *n , double complex *alpha , double complex *a , int *ia , int *ja , 
               int *desca , double complex *beta , double complex *c , int *ic , int *jc , int *descc );

//__________________________________________________________________________________________________
  
int main(int argc, char** argv) {
  setbuf(stdout, NULL);
  
  // matrix and block size
  int N, nblk;

  // MPI
  int world_rank, world_size;
  int na_cols, na_rows; // local matrix size
  int np_cols, np_rows; // number of MPI processes per column/row
  int my_prow, my_pcol; // local MPI task position (my_prow, my_pcol) in the grid (0..np_cols -1, 0..np_rows -1)
  int provided_mpi_thread_level;

  // Matrices
  double complex *a, *at;

  int iZERO=0, iONE=1;

  //__________________________________________________________________________________________________
  // MPI

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  //__________________________________________________________________________________________________
  
  if (argc == 3) 
    {
    N = atoi(argv[1]);
    nblk = atoi(argv[2]);
    } 
  else 
    {
    N = 64; // 64 is the minimal size for which the problem is observed
    nblk = 32;
    }

  np_cols = 1;
  np_rows = 2;

  if (world_rank == 0)
    {
    printf("Matrix size: %i\n", N);
    printf("Blocksize: %i\n", nblk);
    printf("Num MPI proc: %i\n", world_size);
    printf("Number of process rows=%i, cols=%i, total=%i\n", np_rows, np_cols, world_size);
    printf("sizeof(double complex)=%d\n", sizeof(double complex));
    }

  //__________________________________________________________________________________________________
  // Setup BLACS

  int sc_desc[9], info, blacs_ok;
  int ictxt, my_blacs_ctxt, np_rows_blacs, np_cols_blacs;

  // Initialize BLACS context
  Cblacs_pinfo(&world_rank, &world_size);
  Cblacs_get(-1, 0, &ictxt);
  Cblacs_gridinit(&ictxt, "Col", np_rows, np_cols);
  Cblacs_gridinfo(ictxt, &np_rows_blacs, &np_cols_blacs, &my_prow, &my_pcol);

  // Define matrix dimensions
  na_rows = numroc_(&N, &nblk, &my_prow, &iZERO, &np_rows);  // Local rows
  na_cols = numroc_(&N, &nblk, &my_pcol, &iZERO, &np_cols);  // Local columns

  descinit_(sc_desc, &N, &N, &nblk, &nblk, &iZERO, &iZERO, &ictxt, &na_rows, &info);

  //__________________________________________________________________________________________________
  // Setup NCCL
  
  cudaSetDevice(world_rank);

  cudaStream_t my_stream;
  cudaStreamCreate(&my_stream);

  ncclUniqueId id;
  if (world_rank == 0) ncclGetUniqueId(&id);
  MPI_Bcast(&id, sizeof(id), MPI_BYTE, 0, MPI_COMM_WORLD);

  // create the NCCL communicator:
  ncclComm_t comm;
  ncclCommInitRank(&comm, world_size, id, world_rank);

  //__________________________________________________________________________________________________
  // Allocate the arrays

  a  = (double complex *) calloc(na_rows*na_cols, sizeof(double complex));
  at = (double complex *) calloc(na_rows*na_cols, sizeof(double complex));
    
  double complex *tmp1_dev;
  size_t num = 1;
  cudaMalloc((void **)&tmp1_dev,  num*sizeof(double complex));
  cudaMemset((void *)tmp1_dev, 0, num*sizeof(double complex));

  //==================================================================================================
  // PZTRANC
    
  for(int i=0; i<9; i++) printf("sc_desc[%d]=%d \n", i, sc_desc[i]);
  printf("\n");

  double complex alpha = 1.0, beta = 0.0;

  pztranc_(&N, &N, &alpha, a, &iONE, &iONE, sc_desc, &beta, at, &iONE, &iONE, sc_desc);
  printf("pztranc_ completed, world_rank=%d\n", world_rank);
  
  // MPI_Barrier(MPI_COMM_WORLD); //  when uncommented --> resolves the hang
  // printf("MPI_Barrier completed, world_rank=%d\n", world_rank);

  //==================================================================================================
  // NCCL communication

  int root = 0;
  
  // these hang (ncclReduce, ncclAllReduce)
  ncclReduce((const void*)tmp1_dev, (void*)tmp1_dev, 2*num, ncclDouble, ncclSum, root, comm, my_stream);
  //ncclAllReduce((const void*)tmp1_dev, (void*)tmp1_dev, 2*num, ncclDouble, ncclSum, comm, my_stream);
  
  // this works
  //ncclBroadcast((const void*)tmp1_dev, (void*)tmp1_dev, 2*num, ncclDouble, root, comm, my_stream);
  
  //==================================================================================================
  // Deallocate and finalize

  cudaFree(tmp1_dev);

  free(a);
  free(at);

  MPI_Finalize();

  return 0;
}
